# [3.5.0](https://gitlab.com/to-be-continuous/k6/compare/3.4.3...3.5.0) (2025-01-27)


### Features

* disable tracking service by default ([c52c6b3](https://gitlab.com/to-be-continuous/k6/commit/c52c6b331a6fde3d46290b6db879bd40252b3f55))

## [3.4.3](https://gitlab.com/to-be-continuous/k6/compare/3.4.2...3.4.3) (2024-09-05)


### Bug Fixes

* updated links to resolve 404s ([afd5307](https://gitlab.com/to-be-continuous/k6/commit/afd5307b1d3ca7a4682cf53cbc34424851bff364))

## [3.4.2](https://gitlab.com/to-be-continuous/k6/compare/3.4.1...3.4.2) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([4e0622b](https://gitlab.com/to-be-continuous/k6/commit/4e0622be06d9cf9d74a6ca1dcf6b72718d9cb353))

## [3.4.1](https://gitlab.com/to-be-continuous/k6/compare/3.4.0...3.4.1) (2024-2-6)


### Bug Fixes

* **deps:** change deprecated loadimpact/k6 image to grafana/k6 ([b09377f](https://gitlab.com/to-be-continuous/k6/commit/b09377fe2440a1519d4694b5517f67017dd06cdf))

# [3.4.0](https://gitlab.com/to-be-continuous/k6/compare/3.3.0...3.4.0) (2024-1-27)


### Features

* migrate to CI/CD component ([44d6ddf](https://gitlab.com/to-be-continuous/k6/commit/44d6ddf7df979b1ffb8959e23151a647c5a181b5))

# [3.3.0](https://gitlab.com/to-be-continuous/k6/compare/3.2.1...3.3.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([ad4be43](https://gitlab.com/to-be-continuous/k6/commit/ad4be433a70ce549dc709bedf96983c34d461b58))

## [3.2.1](https://gitlab.com/to-be-continuous/k6/compare/3.2.0...3.2.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([5976896](https://gitlab.com/to-be-continuous/k6/commit/5976896992e594a571ec4fc8921fe7764b253324))

# [3.2.0](https://gitlab.com/to-be-continuous/k6/compare/3.1.1...3.2.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([7e372b8](https://gitlab.com/to-be-continuous/k6/commit/7e372b80ea8d7cb0fb5a08feffbe3fbbf21f7b25))

## [3.1.1](https://gitlab.com/to-be-continuous/k6/compare/3.1.0...3.1.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([b54fcad](https://gitlab.com/to-be-continuous/k6/commit/b54fcadd796a081eaa1a40c766149740138d8209))

# [3.1.0](https://gitlab.com/to-be-continuous/k6/compare/3.0.0...3.1.0) (2022-10-04)


### Features

* normalize reports ([d1a5d6f](https://gitlab.com/to-be-continuous/k6/commit/d1a5d6fb1fb11a329f168f1bcb0a4ae2f06a1a42))

# [3.0.0](https://gitlab.com/to-be-continuous/k6/compare/2.1.0...3.0.0) (2022-08-05)


### Features

* adapative pipeline ([347464c](https://gitlab.com/to-be-continuous/k6/commit/347464cbff782af897d2e049e746e6e19494e9eb))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.1.0](https://gitlab.com/to-be-continuous/k6/compare/2.0.1...2.1.0) (2022-05-01)


### Features

* configurable tracking image ([3da1826](https://gitlab.com/to-be-continuous/k6/commit/3da1826da0ae2b4e34bd2af0ac1b19dc6f52d4f1))

## [2.0.1](https://gitlab.com/to-be-continuous/k6/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([78b3ba5](https://gitlab.com/to-be-continuous/k6/commit/78b3ba5f0b52d835dacebcbb360f813cfef97bed))

## [2.0.0](https://gitlab.com/to-be-continuous/k6/compare/1.0.0...2.0.0) (2021-09-03)

### Features

* Change boolean variable behaviour ([f3112ac](https://gitlab.com/to-be-continuous/k6/commit/f3112ac0ae4e84ce4014a220dd5093e395a3aab2))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## 1.0.0 (2021-07-23)

### Features

* add scoped variables support ([a74e805](https://gitlab.com/to-be-continuous/k6/commit/a74e8054d3dc728f27628ade37af2dceee6884e9))
* initialisation of k6 template ([4a38e3e](https://gitlab.com/to-be-continuous/k6/commit/4a38e3ec6a6d3608f25ef985f17d4b2a81774f82))
